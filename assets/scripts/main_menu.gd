
extends ReferenceFrame

func _ready():
	get_node("container/single_player").grab_focus()

func _on_single_player_pressed():
	var game_scene = load("res://assets/scenes/game.scn").instance()
	game_scene.withAi(false, true)
	get_parent().add_child(game_scene)
	get_parent().remove_child(self)

func _on_multi_player_pressed():
	var game_scene = load("res://assets/scenes/game.scn").instance()
	game_scene.withAi(false, false)
	get_parent().add_child(game_scene)
	get_parent().remove_child(self)


func _on_exit_pressed():
	get_tree().quit()
