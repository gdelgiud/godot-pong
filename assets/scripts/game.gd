
extends Control

const PAD_SPEED = 300
const START_BALL_SPEED = 200
const BALL_ACCELERATION = 0.2
const NATIVE_H_RES = 1280
const NATIVE_V_RES = 720

var pad_size
var ball_size
var ball_speed = START_BALL_SPEED
var direction = Vector2(0, 0)
var left_score = 0
var right_score = 0
var paused = false
var left_ai = false
var right_ai = false
var ai_pad_center = 0

func withAi(left_ai, right_ai):
	self.left_ai = left_ai
	self.right_ai = right_ai

func _ready():
	ball_size = get_node("ball").get_texture().get_size()
	pad_size = get_node("left").get_texture().get_size()
	randomize(true)
	
	_calc_field_size(16.0, 9.0)
	_choose_random_ai_center()
	_restart()
	set_process(true)

func _process(delta):
	var field_size = get_size()
	
	if (Input.is_action_pressed("pause")):
		_pause()
	
	# Process left pad input
	if (Input.is_action_pressed("left_up")):
		_move_pad_up(delta, get_node("left"))
	elif (Input.is_action_pressed("left_down")):
		_move_pad_down(delta, get_node("left"))
	
	# Process right pad input
	if right_ai:
		_handle_ai(delta, get_node("right"))
	else:
		if (Input.is_action_pressed("right_up")):
			_move_pad_up(delta, get_node("right"))
		elif (Input.is_action_pressed("right_down")):
			_move_pad_down(delta, get_node("right"))
	
	# Move ball
	var ball_pos = get_node("ball").get_pos()
	ball_pos += direction * ball_speed * delta
	get_node("ball").set_pos(ball_pos)
	
	# Handle ball collision
	var left_rect = Rect2(get_node("left").get_pos() - pad_size / 2, pad_size)
	var right_rect = Rect2(get_node("right").get_pos() - pad_size / 2, pad_size)
	var ball_rect = Rect2(get_node("ball").get_pos() - ball_size / 2, ball_size)
	
	if (left_rect.intersects(ball_rect) and direction.x < 0):
	    _collide_ball_with_pad(get_node("left"))
	elif (right_rect.intersects(ball_rect) and direction.x > 0):
		_collide_ball_with_pad(get_node("right"))
	
	# Bounce with top and bottom border
	if ((ball_pos.y - ball_size.y / 2 < 0 and direction.y < 0) or (ball_pos.y + ball_size.y / 2 > field_size.y and direction.y > 0)):
    	direction.y *= -1

	# Reset game if the ball left the screen
	if (ball_pos.x < 0):
		_score_right()
	elif (ball_pos.x > field_size.x):
		_score_left()
		
func _draw():
	# Draw background
	var rect = get_item_rect()
	var rect_size = rect.size
	draw_rect(rect, Color(0.45, 0.5, 0.55))
	draw_line(Vector2(rect_size.x / 2, 0), Vector2(rect_size.x / 2, rect_size.y), Color(1.0, 1.0, 1.0), 1.0)

func _pause():
	get_tree().set_pause(true)
	get_node("pause_popup").show()

func _unpause():
	get_tree().set_pause(false)
	get_node("pause_popup").hide()

func _calc_field_size(width_ratio, height_ratio):
	var screen_size = get_viewport_rect().size
	var field_size = Vector2(0, 0)
	var ratio = width_ratio / height_ratio
	if (screen_size.x / screen_size.y != ratio):
		if (screen_size.x < screen_size.y * ratio):
			field_size.x = screen_size.x
			field_size.y = field_size.x * 1 / ratio
		else:
			field_size.y = screen_size.y
			field_size.x = field_size.y * ratio
	else:
		field_size = screen_size
		
	set_size(Vector2(NATIVE_H_RES, NATIVE_V_RES))
	set_scale(field_size / get_size())
	set_pos((screen_size - field_size) / 2)

func _handle_ai(delta, pad):
	var ball_pos = get_node("ball").get_pos()
	var pad_pos = pad.get_pos()
	var ai_center_offset = ai_pad_center * pad_size.y / 2
	if (ball_pos.y > pad_pos.y + ai_center_offset):
		_move_pad_down(delta, pad)
	elif (ball_pos.y < pad_pos.y  - ai_center_offset):
		_move_pad_up(delta, pad)

func _choose_random_ai_center():
	ai_pad_center = randf() * 2.0 - 1

func _collide_ball_with_pad(pad):
	direction.x = -direction.x
	ball_speed *= 1 + BALL_ACCELERATION
	direction.y = 1.5 * ((get_node("ball").get_pos().y - pad.get_pos().y) / pad_size.y + (randf() * 0.2 - 0.1))
	direction = direction.normalized()
	_choose_random_ai_center()

func _move_pad_up(delta, pad):
	var pos = pad.get_pos()
	if (pos.y - pad_size.y / 2 > 0):
		pos.y -= PAD_SPEED * delta
	else:
		pos.y = pad_size.y / 2
	pad.set_pos(pos)
	
func _move_pad_down(delta, pad):
	var pos = pad.get_pos()
	var field_size = get_size()
	
	if (pos.y + pad_size.y / 2 < field_size.y):
		pos.y += PAD_SPEED * delta
	else:
		pos.y = field_size.y - pad_size.y / 2
	pad.set_pos(pos)
	
func _score_left():
	left_score += 1
	_restart()
	
func _score_right():
	right_score += 1
	_restart()
	
func _restart():
	var field_size = get_size()
	
	# Reset left pad
	var left_node = get_node("left")
	var left_pos = left_node.get_pos()
	left_pos.y = field_size.y / 2
	left_node.set_pos(left_pos)
	
	# Reset right pad
	var right_node = get_node("right")
	var right_pos = right_node.get_pos()
	right_pos.y = field_size.y / 2
	right_node.set_pos(right_pos)
	
	direction = Vector2((randi() % 2) * 2.0 - 1, 0)
	ball_speed = START_BALL_SPEED
	get_node("ball").set_pos(field_size * 0.5)
	
	get_node("left_score").set_text(str(left_score))
	get_node("right_score").set_text(str(right_score))

func _on_game_resized():
	_calc_field_size(16.0, 9.0)

func _main_menu():
	get_tree().set_pause(false)
	var main_menu_scene = load("res://assets/scenes/main_menu.scn").instance()
	get_parent().add_child(main_menu_scene)
	get_parent().remove_child(self)

func _exit():
	get_tree().set_pause(false)
	get_tree().quit()


func _on_main_menu_pressed():
	_main_menu()


func _on_exit_pressed():
	_exit()
