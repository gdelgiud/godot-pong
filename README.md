# Godot Pong

## Description
Yet another Pong clone, made with [Godot Engine](https://godotengine.org/). It can be played with a friend, or against the computer.

## Controls

The game can be played either with a keyboard or with two gamepads.

+ **UP**: move right pad up
+ **DOWN**: move right pad down
+ **W**: move left pad up
+ **S**: move left pad down

## Downloads

This game is available for Windows, Linux and Mac

+ [Windows (64 bits)](https://drive.google.com/open?id=0B1hBSum2R2meT1gxNmxzcWEzWWc)
+ [Linux (64 bits)](https://drive.google.com/open?id=0B1hBSum2R2meWmtMOFh4V1ZFY2M)
+ [Mac](https://drive.google.com/open?id=0B1hBSum2R2meSEt4by1QbUg1RVE)

## Screenshot

![screenshot](https://gitlab.com/gdelgiud/godot-pong/raw/b9703c24a2fa9d19179167ef956ddc6e2e5d990c/screenshots/screenshot.png)